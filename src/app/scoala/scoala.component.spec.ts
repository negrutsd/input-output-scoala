import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ScoalaComponent } from './scoala.component';

describe('ScoalaComponent', () => {
  let component: ScoalaComponent;
  let fixture: ComponentFixture<ScoalaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ScoalaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ScoalaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
