import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-scoala',
  templateUrl: './scoala.component.html',
  styleUrls: ['./scoala.component.css']
})
export class ScoalaComponent implements OnInit {

  public tasks: Task = new Task();

  constructor() { }

  ngOnInit() {
  }
}

export class Task {
  public message: string;
}
