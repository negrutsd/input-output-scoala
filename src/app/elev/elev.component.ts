import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Task } from '../scoala/scoala.component';

@Component({
  selector: 'app-elev',
  templateUrl: './elev.component.html',
  styleUrls: ['./elev.component.css']
})
export class ElevComponent implements OnInit {

  @Input() public taske: Task;
  @Output() public elevEvent: EventEmitter<Task> = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }
  onClick() {
    this.taske.message = 'Message sent';
    this.elevEvent.emit(this.taske);
  }

}
