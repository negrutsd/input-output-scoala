import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Task } from '../scoala/scoala.component';

@Component({
  selector: 'app-rector',
  templateUrl: './rector.component.html',
  styleUrls: ['./rector.component.css']
})
export class RectorComponent implements OnInit {

  @Input() taskr: Task;
  @Output() rectorEvent: EventEmitter<Task> = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }
  rectorTask() {
    this.rectorEvent.emit(this.taskr);
  }

}
