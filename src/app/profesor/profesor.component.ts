import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Task } from '../scoala/scoala.component';

@Component({
  selector: 'app-profesor',
  templateUrl: './profesor.component.html',
  styleUrls: ['./profesor.component.css']
})
export class ProfesorComponent implements OnInit {

  @Input() public taskp: Task;
  @Output() public profesorEvent: EventEmitter<Task> = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }
  onTaskReceived() {
    this.profesorEvent.emit(this.taskp);
  }

}
