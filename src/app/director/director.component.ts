import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Task } from '../scoala/scoala.component';

@Component({
  selector: 'app-director',
  templateUrl: './director.component.html',
  styleUrls: ['./director.component.css']
})
export class DirectorComponent implements OnInit {

  @Input() public taskd: Task;
  @Output() public directorEvent: EventEmitter<Task> = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }
  onTask() {
    this.directorEvent.emit(this.taskd);
  }

}
