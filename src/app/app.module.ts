import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { ScoalaComponent } from './scoala/scoala.component';
import { ProfesorComponent } from './profesor/profesor.component';
import { ElevComponent } from './elev/elev.component';
import { DirectorComponent } from './director/director.component';
import { RectorComponent } from './rector/rector.component';

@NgModule({
  declarations: [
    AppComponent,
    ScoalaComponent,
    ProfesorComponent,
    ElevComponent,
    DirectorComponent,
    RectorComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
